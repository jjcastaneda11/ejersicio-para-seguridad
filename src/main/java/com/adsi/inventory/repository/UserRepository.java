package com.adsi.inventory.repository;

import com.adsi.inventory.domain.Users;
import com.adsi.inventory.service.dto.UsersDTO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository  extends JpaRepository<Users, Long> {

   Optional<Users> findById (Long id);

}
