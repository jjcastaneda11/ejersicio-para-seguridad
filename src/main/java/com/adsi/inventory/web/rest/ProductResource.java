package com.adsi.inventory.web.rest;

import com.adsi.inventory.domain.Product;
import com.adsi.inventory.service.ProductService;
import com.adsi.inventory.service.dto.ProductDTO;
import com.adsi.inventory.service.imp.ProductServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductResource {

    @Autowired
    ProductService productService;

    @GetMapping("/product")
    public ResponseEntity<List<ProductDTO>> getAll(){
        return ResponseEntity.status(200).body(productService.getAll());
    }


    @GetMapping("/product/{reference}")
    public ResponseEntity<ProductDTO> findByReference(@PathVariable String reference){
        return new ResponseEntity<>(productService.getByReference(reference), HttpStatus.OK);
    }

    @PostMapping("/product")
    public ResponseEntity<ProductDTO> create(@RequestBody ProductDTO productDTO) {
        return new ResponseEntity<>(productService.save(productDTO), HttpStatus.CREATED);
    }

}
