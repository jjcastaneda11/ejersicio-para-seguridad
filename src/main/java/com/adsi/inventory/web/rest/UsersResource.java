package com.adsi.inventory.web.rest;

import com.adsi.inventory.service.UsersService;
import com.adsi.inventory.service.dto.ProductDTO;
import com.adsi.inventory.service.dto.UsersDTO;
import com.adsi.inventory.service.imp.UsersServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UsersResource {

    @Autowired
    UsersService usersService;

    @PostMapping("/user")
    public ResponseEntity<UsersDTO> create(@RequestBody UsersDTO usersDTO) {
        return new ResponseEntity<>(usersService.save(usersDTO), HttpStatus.CREATED);
    }

    @GetMapping("/user")
    public ResponseEntity<List<UsersDTO>> getAll(){
        return ResponseEntity.status(200).body(usersService.getAll());
    }

    @GetMapping("/user{id}")
    public ResponseEntity<UsersDTO> findById(@PathVariable Long id){
        return new ResponseEntity<>(usersService.getById(id), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody UsersDTO usersDTO) {
        return  usersService.update(usersDTO);
    }


}
