package com.adsi.inventory.service;

import com.adsi.inventory.service.dto.ProductDTO;

import java.util.List;

public interface ProductService {

    //Listar todos los productor
    public List<ProductDTO> getAll();

    //Guardar un nuevo producto
    public ProductDTO save(ProductDTO productDTO);

    public ProductDTO getByReference(String reference);

}
