package com.adsi.inventory.service.imp;

import com.adsi.inventory.repository.ProductRepository;
import com.adsi.inventory.repository.UserRepository;
import com.adsi.inventory.service.UsersService;
import com.adsi.inventory.service.dto.ProductTransformer;
import com.adsi.inventory.service.dto.UsersDTO;
import com.adsi.inventory.service.dto.UsersTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UsersServiceImp implements UsersService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public UsersDTO save(UsersDTO usersDTO) {
        return UsersTransformer.getUsersDTOFromUsers(userRepository.save(UsersTransformer.getUsersFromUsersDTO(usersDTO)));
    }

    @Override
    public List<UsersDTO> getAll() {
        return userRepository.findAll().stream()
                .map(UsersTransformer::getUsersDTOFromUsers)
                .collect(Collectors.toList());
    }

    @Override
    public UsersDTO getById(Long id) {
        return UsersTransformer.getUsersDTOFromUsers(userRepository.findById(id).get());
    }

    @Override
    public ResponseEntity update(UsersDTO usersDTO) {
        return new ResponseEntity(userRepository.save(UsersTransformer.getUsersFromUsersDTO(usersDTO)),HttpStatus.OK);
    }
}
