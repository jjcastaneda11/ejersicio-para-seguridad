package com.adsi.inventory.service;

import com.adsi.inventory.domain.Users;
import com.adsi.inventory.service.dto.ProductDTO;
import com.adsi.inventory.service.dto.UsersDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface UsersService {

    public UsersDTO save(UsersDTO usersDTO);

    //Listar todos los productor
    public List<UsersDTO> getAll();

    public UsersDTO getById (Long id);

    public ResponseEntity update(UsersDTO usersDTO);

}
